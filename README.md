# README #

MaSyMoS project description:
https://sems.uni-rostock.de/projects/masymos/

### What is this repository for? ###

* Quick summary
MaSyMoS is a Neo4J based application to store SBML and CellML Models as well as Simulation Descriptions. All data is represented as a Graph and can be queried using Cypher. A description of the underlying database is available from our latest publication on https://peerj.com/preprints/376v2/ and http://link.springer.com/chapter/10.1007/978-3-319-08590-6_8 .



### How do I get set up? ###

* Summary of set up
Clone the repository (e.g. using Eclipse EGit)

* Dependencies
managed by Maven


### Contribution guidelines ###

* Code: 
Ron Henkel and Rebekka Alm

* Other guidelines: 
Helpful for writing Cypher queries: http://docs.neo4j.org/refcard/2.0/

### Who do I talk to? ###

* Repo owner
Ron Henkel, ron (dot) henkel (at) uni-rostock (dot) de