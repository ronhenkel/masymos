package de.unirostock.sems.masymos.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import de.unirostock.sems.masymos.configuration.Config;
import de.unirostock.sems.masymos.data.PersonWrapper;
import de.unirostock.sems.masymos.database.Manager;
import de.unirostock.sems.masymos.query.IQueryInterface;
import de.unirostock.sems.masymos.query.QueryAdapter;
import de.unirostock.sems.masymos.query.enumerator.AnnotationFieldEnumerator;
import de.unirostock.sems.masymos.query.enumerator.CellMLModelFieldEnumerator;
import de.unirostock.sems.masymos.query.enumerator.PersonFieldEnumerator;
import de.unirostock.sems.masymos.query.enumerator.PublicationFieldEnumerator;
import de.unirostock.sems.masymos.query.enumerator.SBMLModelFieldEnumerator;
import de.unirostock.sems.masymos.query.enumerator.SedmlFieldEnumerator;
import de.unirostock.sems.masymos.query.results.AnnotationResultSet;
import de.unirostock.sems.masymos.query.results.ModelResultSet;
import de.unirostock.sems.masymos.query.results.PersonResultSet;
import de.unirostock.sems.masymos.query.results.PublicationResultSet;
import de.unirostock.sems.masymos.query.results.SedmlResultSet;
import de.unirostock.sems.masymos.query.structure.StructureQuery;
import de.unirostock.sems.masymos.query.types.AnnotationQuery;
import de.unirostock.sems.masymos.query.types.CellMLModelQuery;
import de.unirostock.sems.masymos.query.types.PersonQuery;
import de.unirostock.sems.masymos.query.types.PublicationQuery;
import de.unirostock.sems.masymos.query.types.SBMLModelQuery;
import de.unirostock.sems.masymos.query.types.SedmlQuery;
import de.unirostock.sems.masymos.util.ModelResultSetWriter;
import de.unirostock.sems.masymos.util.ResultSetUtil;
import de.unirostock.sems.masymos.util.SBGN2Cypher;

public class MainQuery {
	
	private static String dumpPath = null;

	public static void main(String[] args) {

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-dbPath")) {
				Config.instance().setDbPath(args[++i]);
			} 
			else if (args[i].equals("-dumpPath")) {
				dumpPath = args[++i];
			}
		}

		// create neo4j database
		long start = System.currentTimeMillis();
		System.out.println("Started at: " + new Date());
		System.out.print("Getting manager...");
		Manager.instance();
		System.out.println("done in " + (System.currentTimeMillis() - start)
				+ "ms");
		System.out.println();

		String s = "";
		while (!s.equals("exit")) {
			System.out
					.println("QueryType: (1) sbmlmodeliq, (2) cellmlmodeliq, (3) annoiq->model, (4) persiq->model, "
							+ "(5) pubiq->model, (6) annoiq, (7) persiq, (8) pubiq, "
							+ "(9) all, (10) cypher, (11) sedml, (12) sbgn: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}

			switch (Integer.valueOf(s)) {
				case 1:
					sbmlModelInterfaceQuery(); break;
				case 2:
					cellmlModelInterfaceQuery(); break;	
				case 3: 
					annoToModelInterfaceQuery(); break;
				case 4:
					personToModelInterfaceQuery(); break;
				case 5:
					publicationToModelInterfaceQuery(); break;
				case 6: 
					annoNativeInterfaceQuery(); break;
				case 7:
					personNativeInterfaceQuery(); break;
				case 8:
					publicationNativeInterfaceQuery(); break;					
				case 9:
					allInterfaceQuery(); break;	
				case 10:
					structureQuery(); break;
				case 11:
					sedmlNativeQuery(); break;	
				case 12:
					sbgnQuery(); break;		
			default:
				continue;
			}
		}
		System.exit(0);
	}

	

	private static void sbgnQuery() {
		
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Path to SBGN file: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			try {
				System.out.println(SBGN2Cypher.toCypher(s));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}


	private static void annoToModelInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from anno interface...");
			List<ModelResultSet> results = null;
			try {
				AnnotationQuery aq = new AnnotationQuery();
				aq.setBestN(20);
				aq.setThreshold(0.01f);
				aq.addQueryClause(AnnotationFieldEnumerator.RESOURCETEXT, s);
				List<IQueryInterface> aqL = new LinkedList<IQueryInterface>();
				aqL.add(aq);
				results = QueryAdapter.executeMultipleQueriesForModels(aqL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printModelResults(results);
			System.out.println("done");
		}

	}
	
	private static void annoNativeInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from anno interface...");
			List<AnnotationResultSet> results = null;
			try {
				AnnotationQuery aq = new AnnotationQuery();
				aq.setBestN(20);
				aq.setThreshold(0.01f);
				aq.addQueryClause(AnnotationFieldEnumerator.RESOURCETEXT, s);
				//List<IQueryInterface> aqL = new LinkedList<IQueryInterface>();
				//aqL.add(aq);
				results = QueryAdapter.executeAnnotationQuery(aq);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printAnnotationResults(results);
			System.out.println("done");
		}

	}
	
	private static void personToModelInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from person interface...");
			List<ModelResultSet> results = null;
			try {
				PersonQuery pq = new PersonQuery();
				pq.addQueryClause(PersonFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				qL.add(pq);
				results = QueryAdapter.executeMultipleQueriesForModels(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printModelResults(results);
			System.out.println("done");
		}	
	}

	private static void personNativeInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from person interface...");
			List<PersonResultSet> results = null;
			try {
				PersonQuery pq = new PersonQuery();
				pq.addQueryClause(PersonFieldEnumerator.NONE, s);
				results = QueryAdapter.executePersonQuery(pq);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printPersonResults(results);
			System.out.println("done");
		}	
	}

	
	private static void sbmlModelInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from SBML model interface...");
			List<ModelResultSet> results = null;
			try {
				SBMLModelQuery pq = new SBMLModelQuery();
				pq.addQueryClause(SBMLModelFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				qL.add(pq);
				results = QueryAdapter.executeMultipleQueriesForModels(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printModelResults(results);
			System.out.println("done");
		}

	}
	
	private static void cellmlModelInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from CellML model interface...");
			List<ModelResultSet> results = null;
			try {
				CellMLModelQuery pq = new CellMLModelQuery();
				pq.addQueryClause(CellMLModelFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				qL.add(pq);
				results = QueryAdapter.executeMultipleQueriesForModels(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printModelResults(results);
			System.out.println("done");
		}
		
		
	}
	
	private static void publicationToModelInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from publication interface...");
			List<ModelResultSet> results = null;
			try {
				PublicationQuery pq = new PublicationQuery();
				pq.addQueryClause(PublicationFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				qL.add(pq);
				results = QueryAdapter.executeMultipleQueriesForModels(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printModelResults(results);
			System.out.println("done");
		}

	}
	
	private static void publicationNativeInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from publication interface...");
			List<PublicationResultSet> results = null;
			try {
				PublicationQuery pq = new PublicationQuery();
				pq.addQueryClause(PublicationFieldEnumerator.NONE, s);
				results = QueryAdapter.executePublicationQuery(pq);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printPublicationResults(results);
			System.out.println("done");
		}

	}
	
	/*
	private static void allInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from all interfaces...");
			List<ModelResultSet> results = null;
			try {
				AnnotationQuery aq = new AnnotationQuery();
				aq.setBestN(20);
				//aq.setThreshold(0.01f);
				aq.addQueryClause(AnnotationFieldEnumerator.RESOURCETEXT, s);
				
				PersonQuery ppq = new PersonQuery();
				ppq.addQueryClause(PersonFieldEnumerator.NONE, s);
				
				SBMLModelQuery sq = new SBMLModelQuery();
				sq.addQueryClause(SBMLModelFieldEnumerator.NONE, s);
				
				CellMLModelQuery cq = new CellMLModelQuery();
				cq.addQueryClause(CellMLModelFieldEnumerator.NONE, s);

				
				PublicationQuery pq = new PublicationQuery();
				pq.addQueryClause(PublicationFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				
				qL.add(pq);
				qL.add(aq);
				qL.add(ppq);
				qL.add(sq);
				qL.add(cq);
				
				results = QueryAdapter.executeMultipleQueriesForModels(qL);
				if (!StringUtils.isEmpty(dumpPath)) ModelResultSetWriter.writeModelResults(results, qL, dumpPath);
				results = ResultSetUtil.collateModelResultSetByModelId(results);
				//results = ResultSetUtil.sortModelResultSetByScore(results);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printModelResults(results);
		}
			System.out.println("done");


	}
	*/
	
	
	/* -----------New Code--------------*/
	
	private static List<Integer> getRankersLengths(List<List<ModelResultSet>> rankersList){
		int s = rankersList.size();
		
		List<Integer> length_rankers = new LinkedList<Integer>();
		for(int i = 0; i < s; i++){
			length_rankers.add(rankersList.get(i).size());
		}
		return length_rankers;
	}
	
	private static int getMeanRankersLengths (List<List<ModelResultSet>> rankersList){
		int s = rankersList.size();
		List<Integer> length_rankers = getRankersLengths(rankersList);
		Collections.sort(length_rankers);
		return length_rankers.get(s/2);
	}
	
	private static int distance(int k, List<ModelResultSet> ranker_1, List<ModelResultSet> ranker_2){
		
		int sum = 0;
		double ranking1OfObject1;
		double ranking1OfObject2;
		double ranking2OfObject1;
		double ranking2OfObject2;
		ModelResultSet object1;
		ModelResultSet object2;
		int c = 0;
		
		for(int i = 0; i < Math.min(k, ranker_1.size()); i++)
			for(int j = 0; j <  Math.min(k, ranker_2.size()); j++){
				object1 = ranker_1.get(i);
				ranking1OfObject1 = i + 1; 
				
				if(ranker_2.contains(object1))
					ranking2OfObject1 = ranker_2.indexOf(object1) + 1;
				else{
					ranking2OfObject1 = k+1;
				}
				
				object2 = ranker_2.get(i);
				ranking2OfObject2 = j + 1; 
				
				if(ranker_1.contains(object2))
					ranking1OfObject2 = ranker_1.indexOf(object1) + 1;
				else{
					ranking1OfObject2 = k+1;
				}
				
				//In the case when object1, object2 both appear in one list and they are both absent in another -> no disagreement
				
				if(((ranking1OfObject1 - ranking1OfObject2) * (ranking2OfObject1 - ranking2OfObject2)) <= 0)
					sum++;
				}
				
			//the normalized distance 
			sum = sum/(k * (k-1)/2);
			return sum;	
	}

	private static double distance_avg(List<List<ModelResultSet>> rankersList, List<ModelResultSet> aggregateRanker){
		int s = rankersList.size();
		double sum = 0;
		
		int mean_length = getMeanRankersLengths(rankersList);
		
		List<ModelResultSet> ranker_i = new LinkedList<ModelResultSet>();
		
		for(int i = 0; i < s; i++){
			ranker_i = rankersList.get(i);
			sum += distance(mean_length, ranker_i, aggregateRanker);
		}
		sum = sum / s;
		return sum;
	}

	private static void swap(List<ModelResultSet> ranker, int i, int j){
		ModelResultSet o1 = ranker.get(i);
		ModelResultSet o2 = ranker.get(j);
	
		ranker.set(j, o1);
		ranker.set(i, o2);
	}

	private static List<ModelResultSet> adj (List<List<ModelResultSet>> rankersList, List<ModelResultSet> aggregateRanker){ //adjacent pairs, based on Ke-tau
		double epsilon_min = Integer.MAX_VALUE;
		int count = 0;
		//boolean changed = false;
		
		while (count < 100){ //repeat for-loop until no further reductions can be performed
			//changed = false;
			for(int i = 0; i < aggregateRanker.size() - 2; i++){
				List<ModelResultSet> tempRanker = aggregateRanker;
				swap(tempRanker, i, i+1);
				double epsilon_av = distance_avg(rankersList, tempRanker);
				if (epsilon_av < epsilon_min){
					aggregateRanker = tempRanker;
					epsilon_min = epsilon_av;
					//changed = true;
				}	
			}
			/*
			if (changed){
				
				count = 0;
			}
			else*/ 
				count++;
		}
		return aggregateRanker;
	}
	
	private static List<ModelResultSet> combMNZ(List<List<ModelResultSet>> rankersList, List<ModelResultSet> aggregateRanker){
		int s = rankersList.size();
		for(ModelResultSet o: aggregateRanker){
			int indexOf_o = aggregateRanker.indexOf(o) + 1; 
			int h = 0;
			float brn_sum = 0;  //Borda rank normalization
			
			for(int i = 0; i < s; i++){ //compute h
				List<ModelResultSet> ranker_i = rankersList.get(i);
				
				if (ranker_i.contains(o)){
					int ranking_o = ranker_i.indexOf(o) + 1;
					h++;
					brn_sum += 1 - ((double)(ranking_o - 1) / aggregateRanker.size());  // brn_i
				}
			}
			
			o.setScore(brn_sum * h);
			aggregateRanker.set(indexOf_o, o);
		}
		return aggregateRanker;
	}
	
	private static List<ModelResultSet> localKemenization(List<List<ModelResultSet>> rankersList, List<ModelResultSet> aggregateRanker){
		int rankersListLength = rankersList.size();
		int aggregateRankerLength = aggregateRanker.size();
		int rankingOfObject1;
		int rankingOfObject2;
		
		for(int i = 1; i < aggregateRankerLength; i++)
			for(int j = 0; j < i; j++){
				int pro = 0;
				int con = 0;
				ModelResultSet object1 = aggregateRanker.get(i);
				ModelResultSet object2 = aggregateRanker.get(j);
				for(int l = 0; l < rankersListLength; l++){
					List<ModelResultSet> ranker_i = rankersList.get(l);
					
					if(ranker_i.contains(object1))
						rankingOfObject1 = ranker_i.indexOf(object1);
					else
						rankingOfObject1 = Integer.MAX_VALUE; 
					
					if(ranker_i.contains(object2))
						rankingOfObject2 = ranker_i.indexOf(object2);
					else
						rankingOfObject2 = Integer.MAX_VALUE; 
					
					if(rankingOfObject1 > rankingOfObject2)
						pro++;
					else
						con++;
				}
				if(con > pro){
					swap(aggregateRanker, i, j);
				}
				else if (pro >= con)
					break;
				
			}
		return aggregateRanker;
	}
	
	private static List<ModelResultSet> supervisedLocalKemenization ( List<List<ModelResultSet>> rankersList, List<ModelResultSet> aggregateRanker, HashMap<Integer, Integer> weights){
		
		int s = rankersList.size();
		int aggregateRankerLength = aggregateRanker.size();
		boolean[][] M = new boolean[aggregateRankerLength][aggregateRankerLength];
	
		int rankingOfObject1;
		int rankingOfObject2;
		
		int weightsSum = 0;
		for(int o: weights.values())
			weightsSum += o;
	
		
		for(int i = 0; i < aggregateRankerLength; i++){
			for(int j = i + 1 ; j < aggregateRankerLength; j++){
				ModelResultSet object1 = aggregateRanker.get(i);
				ModelResultSet object2 = aggregateRanker.get(j);
				int score = 0;
				for(int l = 0; l < s; l++){
					List<ModelResultSet> ranker_i = rankersList.get(l);
					
					if(ranker_i.contains(object1))
						rankingOfObject1 = ranker_i.indexOf(object1) + 1;
					else
						rankingOfObject1 = Integer.MAX_VALUE; 
					
					if(ranker_i.contains(object2))
						rankingOfObject2 = ranker_i.indexOf(object2) + 1;
					else
						rankingOfObject2 = Integer.MAX_VALUE; 
					
					if(rankingOfObject1 < rankingOfObject2)
						score += weights.get(l);
				}
				
				if (score > 0.5 * weightsSum){
					M[i][j] = true;
					M[j][i] = false;
				}
				
			}
		}
		for(int x = 0; x < aggregateRankerLength; x++){
			for(int y = x + 1; y < aggregateRankerLength; y++){
				if(M[x][y] == false)
					swap(aggregateRanker, x, y);
			}
		}
		
		return aggregateRanker;
	}
	
	
	private static void allInterfaceQuery() {
			String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from all interfaces...");
			List<ModelResultSet> results = null;
			try {
				AnnotationQuery aq = new AnnotationQuery();
				aq.setBestN(20);
				//aq.setThreshold(0.01f);
				aq.addQueryClause(AnnotationFieldEnumerator.RESOURCETEXT, s);
				
				PersonQuery ppq = new PersonQuery();
				ppq.addQueryClause(PersonFieldEnumerator.NONE, s);
				
				SBMLModelQuery sq = new SBMLModelQuery();
				sq.addQueryClause(SBMLModelFieldEnumerator.NONE, s);
				
				CellMLModelQuery cq = new CellMLModelQuery();
				cq.addQueryClause(CellMLModelFieldEnumerator.NONE, s);

				
				PublicationQuery pq = new PublicationQuery();
				pq.addQueryClause(PublicationFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				
				qL.add(pq);
				qL.add(aq);
				qL.add(ppq);
				qL.add(sq);
				qL.add(cq);
				
				results = QueryAdapter.executeMultipleQueriesForModels(qL);
				if (!StringUtils.isEmpty(dumpPath)) ModelResultSetWriter.writeModelResults(results, qL, dumpPath);
				//results = ResultSetUtil.collateModelResultSetByModelId(results);
				//results = ResultSetUtil.sortModelResultSetByScore(results);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			List<List<ModelResultSet>> rankersList = new LinkedList<List<ModelResultSet>> ();
			
			List<ModelResultSet> modelRanker = new LinkedList<ModelResultSet>();
			List<ModelResultSet> annotationRanker = new LinkedList<ModelResultSet>();
			List<ModelResultSet> personRanker = new LinkedList<ModelResultSet>();
			List<ModelResultSet> publicationRanker = new LinkedList<ModelResultSet>();
			List<ModelResultSet> initialAggregateRanker = new LinkedList<ModelResultSet>();
			
			for(ModelResultSet e : results){
				if (e.getIndexSource().equals("ModelIndex"))
					modelRanker.add(e);
				if (e.getIndexSource().equals("AnnotationIndex"))
					annotationRanker.add(e);
				if (e.getIndexSource().equals("PersonIndex"))
					personRanker.add(e);
				if (e.getIndexSource().equals("PublicationIndex"))
					publicationRanker.add(e);
			}
			
			modelRanker = ResultSetUtil.collateModelResultSetByModelId(modelRanker);
			annotationRanker = ResultSetUtil.collateModelResultSetByModelId(annotationRanker);
			personRanker = ResultSetUtil.collateModelResultSetByModelId(personRanker);
			publicationRanker = ResultSetUtil.collateModelResultSetByModelId(publicationRanker);
			initialAggregateRanker = ResultSetUtil.collateModelResultSetByModelId(results);
			
			rankersList.add(modelRanker);
			rankersList.add(annotationRanker);
			rankersList.add(personRanker);
			rankersList.add(publicationRanker);
			
			results = new LinkedList<ModelResultSet>(); 
			HashMap<Integer, Integer> weights = new HashMap<Integer, Integer>();
			weights.put(0, 4);
			weights.put(1, 3);
			weights.put(2, 1);
			weights.put(3, 1);
		
			final long timeStart = System.currentTimeMillis(); 
			
			if(!(rankersList.isEmpty())){
				//results = adj(rankersList, initialAggregateRanker); //rank aggregation, adjacent pairs (using Kendal-Tau distance)
				//results = combMNZ(rankersList, initialAggregateRanker); //rank aggregation
				//results = localKemenization(rankersList, initialAggregateRanker); //rank aggregation
				results = supervisedLocalKemenization(rankersList, initialAggregateRanker, weights); //rank aggregation
			}
			
			final long timeEnd = System.currentTimeMillis();
			final long time = timeEnd - timeStart;
			
			printModelResults(results);
			
			System.out.println(time + "ms");
			System.out.println("done");
		}

	}
	
	/*----------------*/
	
	private static void sedmlNativeQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from sedml interface...");
			List<SedmlResultSet> results = null;
			try {
				SedmlQuery pq = new SedmlQuery();
				pq.addQueryClause(SedmlFieldEnumerator.NONE, s);
				List<IQueryInterface> qL = new LinkedList<IQueryInterface>();
				
				qL.add(pq);
				results = QueryAdapter.executeSedmlQuery(pq);
				//results = ResultSetUtil.collateModelResultSetByModelId(results);
				//results = ResultSetUtil.sortModelResultSetByScore(results);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printSedmlResults(results);
		}
		
	}
	
	
	private static void structureQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from all interfaces...");
			StructureQuery.runCypherQuery(s);
			System.out.println("===============================================");
		}
	}

	
	private static void printModelResults(List<ModelResultSet> results){
		if ((results != null) && (results.size() > 0)) {
			System.out.println("Found " + results.size() + " results");
			
			for (Iterator<ModelResultSet> iterator = results.iterator(); iterator
					.hasNext();) {
				ModelResultSet resultSet = (ModelResultSet) iterator.next();
				System.out
						.println("===============================================");
				System.out.println(resultSet.getScore());
				System.out.println(resultSet.getModelName());
				System.out.println(resultSet.getModelId());
				System.out.println(resultSet.getVersionId());
				System.out.println(resultSet.getFilename());
				System.out.println(resultSet.getDocumentURI());
				System.out.println(resultSet.getIndexSource());
			}

		} else
			System.out.print("No results!");
		System.out.println();
		System.out
				.println("||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println();
	}
	
	private static void printSedmlResults(List<SedmlResultSet> results){
		if ((results != null) && (results.size() > 0)) {

			System.out.println("Found " + results.size() + " results");
			for (Iterator<SedmlResultSet> iterator = results.iterator(); iterator
					.hasNext();) {
				SedmlResultSet resultSet = (SedmlResultSet) iterator.next();
				System.out
						.println("===============================================");
				System.out.println(resultSet.getScore());
				System.out.println(resultSet.getVersionId());
				System.out.println(resultSet.getFilename());
				System.out.println(resultSet.getFilepath());
				List<String> lmr = resultSet.getModelreferences();
				System.out.println("Model references:");
				for (Iterator<String> iterator2 = lmr.iterator(); iterator2.hasNext();) {
					String mr = (String) iterator2.next();
					System.out.println(mr);
				}
				System.out.println();
			}

		} else
			System.out.print("No results!");
		System.out.println();
		System.out
				.println("||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println();
	}
	
	private static void printAnnotationResults(List<AnnotationResultSet> results){
		if ((results != null) && (results.size() > 0)) {

			System.out.println("Found " + results.size() + " results");
			for (Iterator<AnnotationResultSet> iterator = results.iterator(); iterator
					.hasNext();) {
				AnnotationResultSet resultSet = (AnnotationResultSet) iterator.next();
				System.out
						.println("===============================================");
				System.out.println(resultSet.getScore());
				System.out.println(resultSet.getUri());
				System.out.print("Related models: ");
				for (Iterator<String> models = resultSet.getRelatedModelsURI().iterator(); models.hasNext();) {
					String model = (String) models.next();
					System.out.print(model + ", ");
				}
				System.out.println();				
			}

		} else
			System.out.print("No results!");
		System.out.println();
		System.out
				.println("||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println();
	}
	
	private static void printPersonResults(List<PersonResultSet> results){
		if ((results != null) && (results.size() > 0)) {

			System.out.println("Found " + results.size() + " results");
			for (Iterator<PersonResultSet> iterator = results.iterator(); iterator
					.hasNext();) {
				PersonResultSet resultSet = (PersonResultSet) iterator.next();
				System.out
						.println("===============================================");
				System.out.println(resultSet.getScore());
				System.out.println(resultSet.getFirstName());
				System.out.println(resultSet.getLastName());
				System.out.println(resultSet.getEmail());
				System.out.print("Related models: ");
				for (Iterator<String> models = resultSet.getRelatedModelsURI().iterator(); models.hasNext();) {
					String model = (String) models.next();
					System.out.print(model + ", ");
				}
				System.out.println();				
			}

		} else
			System.out.print("No results!");
		System.out.println();
		System.out
				.println("||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println();
	}
	
	private static void printPublicationResults(List<PublicationResultSet> results){
		if ((results != null) && (results.size() > 0)) {

			System.out.println("Found " + results.size() + " results");
			for (Iterator<PublicationResultSet> iterator = results.iterator(); iterator
					.hasNext();) {
				PublicationResultSet resultSet = (PublicationResultSet) iterator.next();
				System.out
						.println("===============================================");
				System.out.println(resultSet.getScore());
				System.out.println(resultSet.getTitle());
				System.out.println(resultSet.getJounral());
				System.out.println(resultSet.getYear());				
				System.out.println(resultSet.getAffiliation());
				System.out.print("Authors: ");
				for (Iterator<PersonWrapper> persons = resultSet.getAuthors().iterator(); persons.hasNext();) {
					PersonWrapper person = (PersonWrapper) persons.next();
					System.out.print(person.getFirstName() + " " + person.getLastName() + ", ");
				}
				System.out.println();
				System.out.print("Related models: ");
				for (Iterator<String> models = resultSet.getRelatedModelsURI().iterator(); models.hasNext();) {
					String model = (String) models.next();
					System.out.print(model + ", ");
				}
				System.out.println();				
			}

		} else
			System.out.print("No results!");
		System.out.println();
		System.out
				.println("||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println();
	}
	
	

}
