package de.unirostock.sems.masymos.query;

public interface IDocumentResultSetInterface extends IResourceResultSetInterface {
	
	public String getVersionId();
}
