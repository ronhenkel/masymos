package de.unirostock.sems.masymos.query;

public interface IResourceResultSetInterface {
	
	public float getScore();
	
	public String getSearchExplanation();
}
